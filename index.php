<?php 
  /*$target_dir = "upload/";
  $target_file = $target_dir . "sample.csv";
 
  if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    } */
if(isset($_POST['submit'])){
  $csv = array();
  if($_FILES['file']['error'] == 0){
   
    $name = $_FILES['file']['name'];
    $ext = strtolower(end(explode('.', $_FILES['file']['name'])));
    $type = $_FILES['file']['type'];
    $tmpName = $_FILES['file']['tmp_name'];
 

    // check the file is a csv
    if($ext === 'csv'){
        if(($handle = fopen($tmpName, 'r')) !== FALSE) {
            // necessary if a large csv file
            set_time_limit(0);

            $row = 0;

            while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                // number of fields in the csv
                $col_count = count($data);
                
                // get the values from the csv
                if($row !=0){
                
		$csv[$row]['item'] = $data[0];
                $csv[$row]['amount'] = $data[1];
                $csv[$row]['unit'] = $data[2];
		$csv[$row]['use-by'] = $data[3];
               }

                // inc the row
                $row++;
            }
            fclose($handle);
        }
    }
}
$string = file_get_contents("recipt.json");
$json_a = json_decode($string, true);
/*echo "ON fredge : ";
echo "<pre>";
print_r($csv);
echo "</br>";
echo "Receipt : ";
echo "<pre>";
print_r($json_a);*/

$row_count_recipt=count($json_a);
$can_cook = array();
$item_used_by_item = array();
for($i= 0 ; $i<$row_count_recipt;$i++)
{
    $ingredients = $json_a[$i]['ingredients'];
    $no_of_ingred_for_recipt = count($ingredients);
    $no_item_available_for_recipt = 0;
    $recipt = $json_a[$i]['name'];
	$amount = 0;
    foreach($ingredients as $ingred)
    {
        $item = $ingred['item'];
        $amount = $ingred['amount'];
        $unit = $ingred['unit'];
        
       foreach($csv as $it)
       {
          if($item==$it['item'])
          {
             if($amount< $it['amount'] && $unit ==$it['unit'])
             {
                 $no_item_available_for_recipt++; 
                 //echo $item." available on freeze for ".$recipt."<br/>";
                 $date = str_replace('/', '-', $it['use-by']);
 
                 $item_user_by_item[$recipt][] = date('Y-m-d', strtotime($date));
             }
                     
          }
       } 
    }
    if($no_of_ingred_for_recipt==$no_item_available_for_recipt)
    {
       $can_cook[]=$recipt;
    }
}
/*echo "can cook:";
echo "<pre>";
print_r($item_user_by_item);*/
$date =array();

foreach($item_user_by_item as $key=>$row)
{
   if(is_array($row)) 
      $date[$key] = min($row);
}
$min_key = array_search(min($date), $date);
}
?>
<html>
   <head>
	<title>Restaurant</title>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
	<style type="text/css">
		body{
			background: #eee;
			color: #000;
			font-family: 'Roboto', sans-serif;
			font-size: 16px;
			font-weight: 600;
		}
		.form-upload{
			width: 400px;
			min-height: 300px;
			margin: 40px auto;
			border-top: 4px solid #000;
			background: #fff;
			box-shadow: 0px 0px 2px #d8d8d8;
			padding: 15px;
			box-sizing:border-box;
		}
		.custom-upload {
		    background-color: #008000;
		    border: 1px solid #006400;
		    border-radius: 4px;
		    cursor: pointer;
		    color: #fff;
		    padding: 4px 10px;
		    font-size: 16px;
		}
		.custom-upload input {
		    left: -9999px; 
		    position: absolute;
		}
		.text-center{
			text-align: center;
		}
		.form-upload h3{
			letter-spacing: 1px;
			margin-top: 0px;
			margin-bottom: 30px;
		}
		.submit-new{
			background-color: #008000;
		    border: 1px solid #006400;
		    border-radius: 4px;
		    cursor: pointer;
		    color: #fff;
		    padding: 4px 10px;
		    float: right;
		}
		.submit-new input[type="submit"]{
			background: transparent;
			border: 0px;
			font-size: 16px;
			font-weight: bold;
			color: #fff;
			display: block;

		}
		.name{
			color: green;
			margin-left: 5px;
		}
		.ingrid{
			margin: 0px;
			padding: 0px;
		}
		.ingrid li{
			list-style-type: square;
			margin-left: 15px;
		}

	</style>
   </head>
   <body>
	<form class="form-upload" action="#" method="post" enctype="multipart/form-data">
		<h3 class="text-center">Choose recepie</h3>
		<label class=""><input type="file" name="file" /></label>
		<label class="submit-new"><input type="submit" name="submit" value="Upload" /></label>
        </form>
		<?php if(isset($row_count_recipt))
		{?>
        <div class="form-upload">
		
	<h3 class="text-center">Result !</h3>
	<label>Suggested Recepie:</label><span class="name"><?php echo $min_key;?></span><br><br>
	<label>Ingridients:</label>
	<ul class="ingrid">
		<?php
		
                    for($i= 0 ; $i<$row_count_recipt;$i++)
			{
			    
			    $recipt = $json_a[$i]['name'];
			    if($recipt == $min_key)
			    {
			      $ingredients = $json_a[$i]['ingredients'];
			      foreach($ingredients as $ingred)
			      {
				$item = $ingred['item'];
				$amount = $ingred['amount'];
				$unit = $ingred['unit'];
				echo "<li>".$item.",".$amount.$unit."</li>"; 
			       
			      }
			    }
			}
	
 		?>
	</ul>
	
</div>
<?php }?>
      
   </body>
</html>


